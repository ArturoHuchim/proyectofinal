for entry in *json
do
  docker cp "$entry" mongo:"/tmp/$entry"
  docker exec mongo mongoimport -d bicimad_4 -c viajes --file /tmp/$entry
  echo "$entry"
done