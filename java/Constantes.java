package edu.comillas.proyecto_final;

public class Constantes {
    public static final String IP = "127.0.0.1";
    public static final int PORT = 27017;
    public static final String DATABASE = "bicimad";
    public static final String COLLECTION_VIAJES = "viajes";
    public static final String COLLECTION_ESTACIONES = "estaciones";
}
