package edu.comillas.proyecto_final;

public class RutaDemanda {
    private int id_estacion_origen;
    private String nombre_estacion_origen;
    private int id_estacion_destino;
    private String nombre_estacion_destino;
    private int numero_movimientos;

    public RutaDemanda(int id_estacion_origen, int id_estacion_destino, String nombre_estacion_origen, String nombre_estacion_destino, int numero_movimientos) {
        this.id_estacion_origen = id_estacion_origen;
        this.id_estacion_destino = id_estacion_destino;
        this.nombre_estacion_origen = nombre_estacion_origen;
        this.nombre_estacion_destino = nombre_estacion_destino;
        this.numero_movimientos = numero_movimientos;
    }

    public int getId_estacion_origen() {
        return id_estacion_origen;
    }

    public void setId_estacion_origen(int id_estacion_origen) {
        this.id_estacion_origen = id_estacion_origen;
    }

    public int getId_estacion_destino() {
        return id_estacion_destino;
    }

    public void setId_estacion_destino(int id_estacion_destino) {
        this.id_estacion_destino = id_estacion_destino;
    }

    public String getNombre_estacion_origen() {
        return nombre_estacion_origen;
    }

    public void setNombre_estacion_origen(String nombre_estacion_origen) {
        this.nombre_estacion_origen = nombre_estacion_origen;
    }

    public String getNombre_estacion_destino() {
        return nombre_estacion_destino;
    }

    public void setNombre_estacion_destino(String nombre_estacion_destino) {
        this.nombre_estacion_destino = nombre_estacion_destino;
    }

    public int getNumero_movimientos() {
        return numero_movimientos;
    }

    public void setNumero_movimientos(int numero_movimientos) {
        this.numero_movimientos = numero_movimientos;
    }
}
