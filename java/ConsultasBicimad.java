package edu.comillas.proyecto_final;

import com.google.gson.Gson;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Accumulators;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Sorts;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.*;

import static com.mongodb.client.model.Projections.*;

public class ConsultasBicimad {
    private MongoClient mongo;
    private MongoCollection<Document> coll;
    private MongoDatabase db1;
    private ArrayList<Estacion> estaciones = null;
    private Gson gson = new Gson();

    public ConsultasBicimad(){
        mongo = new MongoClient(Constantes.IP,Constantes.PORT);
        db1 = mongo.getDatabase(Constantes.DATABASE);
        coll = db1.getCollection(Constantes.COLLECTION_VIAJES);
        estaciones = concultarInformacionEstaciones(db1.getCollection(Constantes.COLLECTION_ESTACIONES));
    }

    public ArrayList<Estacion> concultarInformacionEstaciones(MongoCollection<Document> collEstaciones) {
        ArrayList<Estacion> estacionesAux = new ArrayList<Estacion>();
        MongoCursor<Document> cursor = collEstaciones.find()
                .projection(fields(include("id","name"), excludeId()))
                .iterator();
        try {while (cursor.hasNext()) {
            Document document = cursor.next();
            estacionesAux.add(new Estacion(document.getInteger("id"),document.getString("name")));
        }}
        finally {cursor.close();}
        return estacionesAux;
    }

    private String buscarEstacion(int id){
        for (Estacion estacion:estaciones){
            if(estacion.getId() == id){
                return estacion.getNombre();
            }
        }
        return "N/A";
    }

    public void consultarEstacionesConDemanda(String sentido, String nombreSentido){
        List<Bson> query = new ArrayList<Bson>();
        //query.add(Aggregates.group("$idunplug_station", Accumulators.sum("count",1)));
        query.add(Aggregates.group(sentido, Accumulators.sum("count",1)));
        query.add(Aggregates.sort(Sorts.descending("count")));
        query.add(Aggregates.limit(10));
        MongoCursor<Document> cursor = coll.aggregate(query).iterator();
        System.out.println(nombreSentido);
        try {while (cursor.hasNext()) {
            Document document = cursor.next();
            System.out.println(gson.toJson(new EstacionDemanda(document.getInteger("_id"),buscarEstacion(document.getInteger("_id")),document.getInteger("count"))));
        }}
        finally {cursor.close();}
    }

    public void consultarRutasEnDemanda(){
        Document firstGroup = new Document("$group",
                new Document("_id",new Document("idplug_station", "$idplug_station")
                                        .append("idunplug_station", "$idunplug_station"))
                                        .append("count", new Document("$sum", 1)));
        Document sort = new Document("$sort",new Document("count", -1));
        Document limit = new Document("$limit",10);
        List<Document> pipeline = Arrays.asList(firstGroup,sort,limit);
        MongoCursor<Document> cursor = coll.aggregate(pipeline).iterator();
        try {while (cursor.hasNext()) {System.out.println(cursor.next().toJson());}}
        finally {cursor.close();}
    }

    public static void main(String[] args){
        ConsultasBicimad consultas = new ConsultasBicimad();
        consultas.consultarEstacionesConDemanda("$idunplug_station","Demanda de estacion de origen");
        consultas.consultarEstacionesConDemanda("$idplug_station","Demanda de estacion de destino");
        consultas.consultarRutasEnDemanda();
        consultas.mongo.close();
    }
}
