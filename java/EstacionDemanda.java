package edu.comillas.proyecto_final;

public class EstacionDemanda {
    private int id_estacion;
    private String nombre_estacion;
    private int numero_movimientos;

    public EstacionDemanda(int id_estacion, String nombre_estacion, int numero_movimientos) {
        this.id_estacion = id_estacion;
        this.numero_movimientos = numero_movimientos;
        this.nombre_estacion = nombre_estacion;
    }

    public int getId_estacion() {
        return id_estacion;
    }

    public void setId_estacion(int id_estacion) {
        this.id_estacion = id_estacion;
    }

    public String getNombre_estacion() {
        return nombre_estacion;
    }

    public void setNombre_estacion(String nombre_estacion) {
        this.nombre_estacion = nombre_estacion;
    }

    public int getNumero_movimientos() {
        return numero_movimientos;
    }

    public void setNumero_movimientos(int numero_movimientos) {
        this.numero_movimientos = numero_movimientos;
    }
}
